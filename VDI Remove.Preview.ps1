﻿#----------------------------------------------
# Generated Form Function
#----------------------------------------------
function Show-VDI_Remove_psf {

	#----------------------------------------------
	#region Import the Assemblies
	#----------------------------------------------
	[void][reflection.assembly]::Load('System.Windows.Forms, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089')
	[void][reflection.assembly]::Load('System.Data, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089')
	[void][reflection.assembly]::Load('System.Drawing, Version=2.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a')
	#endregion Import Assemblies

	#----------------------------------------------
	#region Generated Form Objects
	#----------------------------------------------
	[System.Windows.Forms.Application]::EnableVisualStyles()
	$VDIRemove = New-Object 'System.Windows.Forms.Form'
	$buttonConnectToVM = New-Object 'System.Windows.Forms.Button'
	$Info = New-Object 'System.Windows.Forms.GroupBox'
	$checkboxDryRun = New-Object 'System.Windows.Forms.CheckBox'
	$ClipboardButton = New-Object 'System.Windows.Forms.Button'
	$statusbar1 = New-Object 'System.Windows.Forms.StatusBar'
	$labelHost = New-Object 'System.Windows.Forms.Label'
	$Combobox_Host = New-Object 'System.Windows.Forms.ComboBox'
	$Options = New-Object 'System.Windows.Forms.GroupBox'
	$checkboxDDC = New-Object 'System.Windows.Forms.CheckBox'
	$checkboxHDC = New-Object 'System.Windows.Forms.CheckBox'
	$button_DELETE = New-Object 'System.Windows.Forms.Button'
	$labelVDIHostname = New-Object 'System.Windows.Forms.Label'
	$VDIName = New-Object 'System.Windows.Forms.TextBox'
	$InitialFormWindowState = New-Object 'System.Windows.Forms.FormWindowState'
	#endregion Generated Form Objects

	#----------------------------------------------
	#region Generated Form Code
	#----------------------------------------------
	$VDIRemove.SuspendLayout()
	$Options.SuspendLayout()
	#
	# VDIRemove
	#
	$VDIRemove.Controls.Add($buttonConnectToVM)
	$VDIRemove.Controls.Add($Info)
	$VDIRemove.Controls.Add($checkboxDryRun)
	$VDIRemove.Controls.Add($ClipboardButton)
	$VDIRemove.Controls.Add($statusbar1)
	$VDIRemove.Controls.Add($labelHost)
	$VDIRemove.Controls.Add($Combobox_Host)
	$VDIRemove.Controls.Add($Options)
	$VDIRemove.Controls.Add($button_DELETE)
	$VDIRemove.Controls.Add($labelVDIHostname)
	$VDIRemove.Controls.Add($VDIName)
	$VDIRemove.AutoScaleDimensions = '6, 13'
	$VDIRemove.AutoScaleMode = 'Font'
	$VDIRemove.ClientSize = '526, 260'
	$VDIRemove.FormBorderStyle = 'FixedSingle'
	#region Binary Data
	$VDIRemove.Icon = [System.Convert]::FromBase64String('
AAABAAEAEBAAAAAAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAQAQAAAAAAAAAAAAAAAAA
AAAAAAD///8B////Af///wH///8B////Af///wH///8B////Af///wH///8B////Af///wH///8B
////Af///wH///8B////Af///wH///8B////Af///wH///8B////Af///wH///8B////Af///wH/
//8B////Af///wH///8B////AeF/STXigEmf3n5In9t9SJ/afUaf1ntGn9V5RJ/SeESf0HZDn811
Q5/LdUGfy3VBn893Qo3+kUgH////Af///wHfgEdZ5oNL/+GBSf/ch1X/1IBQ/9V5Rf/Qd0T/y3RC
/8NvP/+/bT7/vGs9/79tPv+/bT7/3H1JSf///wH///8B1XtBH+iFTP/lg0v/9NC7//fw6//WmHT/
03lF/+3Ktv////////////38/P/EfVT/v20+/851Q4X///8B////Af///wHohEvj6YVM/+aKVf/1
07///v7+/+G9p//ThFb/2ZBn/9WOZv/RimL/w29A/79tPv/Kc0G/////Af///wH///8B6IRLqeyH
Tf/ohUz/5IJK/+yvjf/9+fb/8OHY/9CIXv/PdkP/y3RC/8dxQf/Cbz//xnFA9f+/PwX///8B////
AeWCS2/wiU7/7IdN/+iETP/kgkr/5JJj//jl2v/7+Pb/1qCA/892Q//Kc0L/xnFA/8JvP//mhEk1
////Af///wHXekQ19ItQ//CJTv/rhk3/54RL/+OCSv/jmW///v7+/+nFsf/SeET/znZD/8pzQv/G
cUD/0nhFb////wH///8BmWYzBfGJTvXzi0//74lO/+uGTf/nlWb/+/Tv//LZyv/af0v/1npG/9J4
RP/OdUP/yXNB/851Q6n///8B////Af///wHwiU6/941R//OLT//tlWP/+vDq//bf0//ihVD/3n9I
/9p8R//VekX/0XdE/811Q//Pd0Pj////Af///wH///8B7YdNhfuPUv/3jVH/++je//jm2//pjFb/
5oNL/+KBSv/dfkj/2XxH/9V6Rf/Rd0T/znZD/+6ITB////8B////Ad9/SEv+kVP/+o9S//msgP/x
klz/7ohO/+qFTP/lg0v/4YBJ/91+SP/ZfEf/1XlF/9B3RP/cfEhZ////Af///wHabUgH7YZNj/CJ
Tp/uiE6f64ZMn+qFS5/mg0uf44FJn+KASZ/efkif3X5In9p9Rp/We0af3H9JNf///wH///8B////
Af///wH///8B////Af///wH///8B////Af///wH///8B////Af///wH///8B////Af///wH///8B
////Af///wH///8B////Af///wH///8B////Af///wH///8B////Af///wH///8B////Af///wH/
//8BAAD//wAA//8AAP//AAD//wAA//8AAP//AAD//wAA//8AAP//AAD//wAA//8AAP//AAD//wAA
//8AAP//AAD//w==')
	#endregion
	$VDIRemove.MaximizeBox = $False
	$VDIRemove.MinimizeBox = $False
	$VDIRemove.Name = 'VDIRemove'
	$VDIRemove.StartPosition = 'CenterParent'
	$VDIRemove.Text = 'VDI Remove'
	$VDIRemove.add_Load($VDIRemove_Load)
	#
	# buttonConnectToVM
	#
	#region Binary Data
	$buttonConnectToVM.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAAC5UlEQVQ4T3WQ2U8T
WBSHz19hMvHF7UV98MFMjEsUXMJkjFtIjA+i8cU4QcGgoG2KUGnRAkKtaCl1gIqtVoIyDgrOxBkS
4hLcMAoCUTFSoUIphVKqIeHz3iJGZ/QkX+5yzvmd371S6L72Qwpc9Q15Z6+Qe8avuPwVfvS9zn+3
0Vp5VSyuejlW7ic2OcXYxOT/0Pc6/10BjfncFTHafQyPf6DpQQ/Nn9H7prYeQtEPGO3eaQE98b9o
B0fLvAyNxXn4Mswjzauw2g/T1hMiGBlH53Vhg/lcHfnqTfln/dMoa5pcxyUMqshQ6sVYdonC3xux
ef7CWtVEX2iMnFO1SJ4qHIxHCU1ECE+MJBiOhQmNhxlSvI+OMDgeIxidItfeSOfbKdp7Y/QGI+SU
eBDTaS99IyFmZbezsLCbnx2vWV0Z4JeaAVJrg6T5Btnnj7HKIph8wu7y1ZTWPuZVf5TsYiVgLKul
6907Fli6WO58w4aLA2ypG2HH9Un2NEL6TTik1iSr0PE2C4MSSXYLLwNTZBepPzCUXuBBz2sWl/aS
5Oln89UIG/+Ebe4U1qumZDV5jWKFWegM5NPavR2DX8jxbeWI7TpytKSGf550sqS8j2WeIEt8URY2
QIpqCoTtPA/+yvOBzTzt30RX8AzOVuFO705Mfwg7bCuQnKIq6lufMtceYE7FEPO8ceYry+sKhPZg
Oq778gXnPaFCrZVtwsP+NNYXCXJICbibHzH6McpoPMJYfJSJjyRstwyspOaFJKjuEM63T1P1TMi9
JWy3LUd+O2ZvOXjCzQGLi/3HnaSbnWSYa0i1LiL5pLBWsU6h/8LTrcS6VPPfQqZnE1nWekTFfMVS
jcnhxXKhGd/tx1y+3UGJ9z5mRzNZ5htssAnegJD3r5BqWUnm8Tr2Gh0tcri4OkFmYaXsyiqozrC6
lJuKhKMMi5u0TItLi6cUCwV3hSST3JoZqNDDv4nZipnkDD8pdMyc5yVOiRD5BBZOcVOYXiHKAAAA
AElFTkSuQmCC')
	#endregion
	$buttonConnectToVM.ImageAlign = 'MiddleLeft'
	$buttonConnectToVM.Location = '127, 169'
	$buttonConnectToVM.Name = 'buttonConnectToVM'
	$buttonConnectToVM.Size = '104, 26'
	$buttonConnectToVM.TabIndex = 0
	$buttonConnectToVM.Text = 'Connect to VM'
	$buttonConnectToVM.TextAlign = 'MiddleRight'
	$buttonConnectToVM.UseVisualStyleBackColor = $True
	#
	# Info
	#
	$Info.Location = '296, 13'
	$Info.Name = 'Info'
	$Info.Size = '218, 215'
	$Info.TabIndex = 14
	$Info.TabStop = $False
	$Info.Text = 'Info'
	#
	# checkboxDryRun
	#
	$checkboxDryRun.Checked = $True
	$checkboxDryRun.CheckState = 'Checked'
	$checkboxDryRun.Location = '12, 204'
	$checkboxDryRun.Name = 'checkboxDryRun'
	$checkboxDryRun.Size = '74, 24'
	$checkboxDryRun.TabIndex = 5
	$checkboxDryRun.Text = 'Dry Run'
	$checkboxDryRun.UseVisualStyleBackColor = $True
	$checkboxDryRun.add_CheckedChanged($checkboxDryRun_CheckedChanged)
	#
	# ClipboardButton
	#
	#region Binary Data
	$ClipboardButton.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAABGdBTUEAALGPC/xhBQAAABl0RVh0
U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAK8SURBVDhPhZLZT1NBFIfnf/FdTXgihqKo
gQgii4LCg74YCa4BN2SRYhFEliBLSw0gAgq9vb1bbwtUWUKCiKiA8qhBQBYRqaVssvycGZAEjXqS
L+fOnHO+mUwuacw4Qp5khJHm9BCSaSoiWbezkZWVBZbZmu2zOutrTA8j9dcNRPd0Es3lJpquE1J/
K1R+fPMgGm4EIS07Hy5XK3TdzTNbs31Wr08LlRvSDpH61EDibGunw1uC2msh8A1JmB9oRl6xGb19
b9DT+4rm18grqoR/oAm+QRE1qUGoSwkk+rNu4nS38mEuqE4xYGnyPd49jEPlIxGWWhssNU9RVdMA
c3Ujhi2RWB7vhzV5D7Q2enV3C1GdOsW5KbBeCYL3Yw/6zfF4a03AoPUkhiyxaLc9gKjqsEkqmh0q
BFmDXVJgd0gUB0RZhqJpMrFeNuDLcCtelMej15yIl5ZE9FXEQtDa8L8QJQmk6pIB4312dJXGobss
gXIK3SVRsKktvGlkdAzjnycokzyPjo1j5NMor7GbEPNFAz501eJ5cSw6SuMpcegoPAqb4uZNU9PT
yM3NhaKokCQZoujAxNQUrwlccMGAYVcp2u7HwFN4gnIcnvxwCFs3mJ39Bp9vnuP1fsfcnBczM7O8
JogiFZwPRr+QAz0/Cq6CGLjuRUM3hW4L2KDJZIKmqvD7F+CgpzIRCy6ooILuuqtQ7h6DmhcNlWbZ
eHhbsLC4iB+rq5zllRUsLi1h3u/ntU1BcjA85WfhuBNBiYQjJwL2zBDYtU0BGzQajXDpOpxOJ30L
hYtYcEE5FcgFcRDooJBzFIIxHE3pByA6W3nT+vo6z79iY2MDa2tr/Js/Yv6ZfZ1lSftRlhRMofmc
ASWnAyC7PbzpX8F+JkJjFyXgd6jgq0NW+Cnsqjuge+wnkhTFR3v/Grspf4h3Qvb+BApKtlis41k4
AAAAAElFTkSuQmCC')
	#endregion
	$ClipboardButton.Location = '237, 26'
	$ClipboardButton.Name = 'ClipboardButton'
	$ClipboardButton.Size = '42, 23'
	$ClipboardButton.TabIndex = 10
	$ClipboardButton.UseVisualStyleBackColor = $True
	$ClipboardButton.add_Click($ClipboardButton_Click)
	#
	# statusbar1
	#
	$statusbar1.Location = '0, 238'
	$statusbar1.Name = 'statusbar1'
	$statusbar1.Size = '526, 22'
	$statusbar1.TabIndex = 13
	$statusbar1.Text = 'statusbar1'
	#
	# labelHost
	#
	$labelHost.AutoSize = $True
	$labelHost.Location = '12, 49'
	$labelHost.Name = 'labelHost'
	$labelHost.Size = '32, 13'
	$labelHost.TabIndex = 10
	$labelHost.Text = 'Host:'
	#
	# Combobox_Host
	#
	$Combobox_Host.AutoCompleteMode = 'Suggest'
	$Combobox_Host.AutoCompleteSource = 'ListItems'
	$Combobox_Host.FormattingEnabled = $True
	[void]$Combobox_Host.Items.Add('HV40')
	[void]$Combobox_Host.Items.Add('HV41')
	[void]$Combobox_Host.Items.Add('HV42')
	[void]$Combobox_Host.Items.Add('HV43')
	$Combobox_Host.Location = '12, 67'
	$Combobox_Host.Name = 'Combobox_Host'
	$Combobox_Host.Size = '219, 21'
	$Combobox_Host.TabIndex = 2
	$Combobox_Host.add_SelectedIndexChanged($Combobox_Host_SelectedIndexChanged)
	#
	# Options
	#
	$Options.Controls.Add($checkboxDDC)
	$Options.Controls.Add($checkboxHDC)
	$Options.Location = '12, 94'
	$Options.Name = 'Options'
	$Options.Size = '219, 69'
	$Options.TabIndex = 3
	$Options.TabStop = $False
	$Options.Text = 'Options'
	#
	# checkboxDDC
	#
	$checkboxDDC.Location = '6, 19'
	$checkboxDDC.Name = 'checkboxDDC'
	$checkboxDDC.Size = '104, 24'
	$checkboxDDC.TabIndex = 3
	$checkboxDDC.Text = 'DDC'
	$checkboxDDC.UseVisualStyleBackColor = $True
	$checkboxDDC.add_CheckedChanged($checkboxDDC_CheckedChanged)
	#
	# checkboxHDC
	#
	$checkboxHDC.Location = '6, 39'
	$checkboxHDC.Name = 'checkboxHDC'
	$checkboxHDC.Size = '104, 24'
	$checkboxHDC.TabIndex = 4
	$checkboxHDC.Text = 'HDC'
	$checkboxHDC.UseVisualStyleBackColor = $True
	$checkboxHDC.add_CheckedChanged($checkboxHDC_CheckedChanged)
	#
	# button_DELETE
	#
	#region Binary Data
	$button_DELETE.Image = [System.Convert]::FromBase64String('
iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACx
jwv8YQUAAAAZdEVYdFNvZnR3YXJlAEFkb2JlIEltYWdlUmVhZHlxyWU8AAAC9klEQVQ4T3XR/U9T
VxjA8fNXzMRNgzJ/cBKJcQuoQCTZMmPGyJZtuChmvmAEdS5WKxAVEKhhMqyIYGFRC71I0SL4Amii
MTEx0UJEg4bBhguFCrS0vRRaKoHvzgF/wM09ySfPuc8597nPyRWmmuv/q9DiaM4738jxc3apYQE7
qq723/ticXWTKLI4xIkKO5PTs4yHpv9D1dX+exsoBZWNItdcz9jEFG3OXtrfUuu2J714g1PkmrX5
BuqL/6YmyD6j4RkP0/Gnj07lL59cj/Gk18twYAK1rw42F1ReJV/eKf+8fZ4cTTlefoUceSi7zCaz
DdPvLZRY71B8sRWXdxzjb3WIPHlwNBzEGwrgC/nnjE368E748EgjQR+vJVfAT06ZRvfAJF39Oq+G
/RhPWxHHzmq4/F4+ONLFStMffFbeT1K1i02Xh/i2boht2iA7NBcv3cOyQQPm+seU1j6iz61zWDXI
PVNHz9AQHxf1sK7qb76wuUm9OsqPzV523vCQdXOUzBYPjwdGMJy7xVddgs2dghevIxwqlVfIKavF
2dvPqrJXbLS6+brJR0qLzvetOttaA2y/HSDrfgRn4VG6E1fQnbSa7oRVPCspZN/ZRkR26WXuP31J
bIWLOOswsZqfGEeImKYwnzQEWWYdx7QnF/cP8WDaDyUGmQ8wsCWRur0ZCOOvF3E8fMYy8yBRFzws
t00Q3TxD9E2Isk+xyDZNX+JHYEyDI2nMGL5jVmayt/L0y1iEQTaoae9EjwTRwwH0kA9d/gUlENbx
MMvzhChor+eduGvHuWkNIvOE+cEvp2o4UGRh/8kq9uVXkplXMU8+7y6p5UXyCmZ2JfHmpw1Mpa+X
OYHpjGQ6Nq9FyIiW1irHyjWKatvR7jnnnNLuYqxyYM3YQV/qp0TS43gjRdLj6fkmjpaUxHvi8OlL
cw6aqsX2Q4WXfi62yGkuzJPrNIPJopq3JcfYO1LicKbE0yHd/jz2mqwvl96JD6W5aRZYLKlYIi2s
LxVCiH8A91OP0FkqVuwAAAAASUVORK5CYII=')
	#endregion
	$button_DELETE.ImageAlign = 'MiddleLeft'
	$button_DELETE.Location = '127, 201'
	$button_DELETE.Name = 'button_DELETE'
	$button_DELETE.Size = '104, 27'
	$button_DELETE.TabIndex = 6
	$button_DELETE.Text = 'DELETE'
	$button_DELETE.TextAlign = 'MiddleRight'
	$button_DELETE.UseVisualStyleBackColor = $True
	$button_DELETE.add_Click($button_DELETE_Click)
	#
	# labelVDIHostname
	#
	$labelVDIHostname.AutoSize = $True
	$labelVDIHostname.Location = '12, 9'
	$labelVDIHostname.Name = 'labelVDIHostname'
	$labelVDIHostname.Size = '79, 13'
	$labelVDIHostname.TabIndex = 3
	$labelVDIHostname.Text = 'VDI Hostname:'
	#
	# VDIName
	#
	$VDIName.Location = '12, 26'
	$VDIName.Name = 'VDIName'
	$VDIName.Size = '219, 20'
	$VDIName.TabIndex = 0
	$Options.ResumeLayout()
	$VDIRemove.ResumeLayout()
	#endregion Generated Form Code

	#----------------------------------------------

	#Save the initial state of the form
	$InitialFormWindowState = $VDIRemove.WindowState
	#Init the OnLoad event to correct the initial state of the form
	$VDIRemove.add_Load($Form_StateCorrection_Load)
	#Clean up the control events
	$VDIRemove.add_FormClosed($Form_Cleanup_FormClosed)
	#Show the Form
	return $VDIRemove.ShowDialog()

} #End Function

#Call the form
Show-VDI_Remove_psf | Out-Null
