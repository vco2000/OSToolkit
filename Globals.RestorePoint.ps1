﻿#--------------------------------------------
# Declare Global Variables and Functions here
#--------------------------------------------


#Sample function that provides the location of the script
function Get-ScriptDirectory
{
<#
	.SYNOPSIS
		Get-ScriptDirectory returns the proper location of the script.

	.OUTPUTS
		System.String
	
	.NOTES
		Returns the correct path within a packaged executable.
#>
	[OutputType([string])]
	param ()
	if ($hostinvocation -ne $null)
	{
		Split-Path $hostinvocation.MyCommand.path
	}
	else
	{
		Split-Path $script:MyInvocation.MyCommand.Path
	}
}


# Font Styles / Schrift Stile
$bold = New-Object Drawing.Font("Lucida Console", 8, [Drawing.Fontstyle]::Bold)
$norm = New-Object Drawing.Font("Lucida Console", 8, [Drawing.Fontstyle]::Regular)
$log = New-Object Drawing.Font("Lucida Console", 1, [Drawing.Fontstyle]::Regular)
[Drawing.Color]$gray = "Control"
[Drawing.Color]$green = "Green"
[Drawing.Color]$red = "Red"
[Drawing.Color]$black = "Black"
$global:Fillchar = 178
$Newline = "`n"
$Newline2 = "`n`n"

#Sample variable that provides the location of the script
[string]$ScriptDirectory = Get-ScriptDirectory

#region Get-ComputerTxtBox
function Get-ComputerTxtBox
{ $global:ComputerName = $textbox_computername.Text }
#endregion
#region Add-RichTextBox
# Function - Add Text to RichTextBox
function Add-RichTextBox
{
	[CmdletBinding()]
	param ($text)
	$Fill = "-"
	$Fill = $Fill * $Fillchar
	#$richtextbox_output.Text += "`tCOMPUTERNAME: $ComputerName`n"
	$richtextbox_output.SelectionFont = $log
	$richtextbox_output.SelectionColor = $Gray
	$timestamp = Get-Date -Format "dd-MM-yyyy HH:mm:ss`n"
	$richtextbox_output.AppendText($timestamp)
	$richtextbox_output.SelectionFont = $norm
	$richtextbox_output.SelectionColor = $Black
	$richtextbox_output.AppendText($text)
	$richtextbox_output.SelectionFont = $bold
	$richtextbox_output.AppendText($Newline)
	$richtextbox_output.AppendText($Fill)
	$richtextbox_output.AppendText($Newline)
}
#Set-Alias artb Add-RichTextBox -Description "Add content to the RichTextBox"
#endregion
#region Add-RichtextBoxTitle
function Add-RichTextBoxTitle
{
	[CmdletBinding()]
	param ($text)
	$Fill = "-"
	$Fill = $Fill * $Fillchar
	#$richtextbox_output.Text += "`tCOMPUTERNAME: $ComputerName`n"
	$richtextbox_output.SelectionFont = $log
	$richtextbox_output.SelectionColor = $Gray
	$timestamp = Get-Date -Format "dd-MM-yyyy HH:mm:ss`n"
	$richtextbox_output.SelectionFont = $bold
	$richtextbox_output.SelectionColor = $Black
	$richtextbox_output.SelectionFont = $bold
	$richtextbox_output.AppendText($text)
	$richtextbox_output.SelectionFont = $bold
	$richtextbox_output.AppendText($Newline)
	$richtextbox_output.AppendText($Fill)
	$richtextbox_output.AppendText($Newline)
}
#endregion
