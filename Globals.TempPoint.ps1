﻿#--------------------------------------------
# Declare Global Variables and Functions here
#--------------------------------------------
# PowershellToolkit information
$ApplicationName = "OS Toolkit"
$ApplicationVersion = "1.0"
$ApplicationLastUpdate = "11.11.2016"

# Author Information
$AuthorName = "Oscar Aguirre"
$AuthorEmail = "vco2000@gmail.com"

# Text to show in the Status Bar when the form load
$StatusBarStartUp = "$ApplicationName - $ApplicationVersion - (c) Oscar Aguirre"

# Title of the MainForm / Mainform Title
$domain = $env:userdomain.ToUpper()
$MainFormTitle = "$ApplicationName $ApplicationVersion - Last Update: $ApplicationLastUpdate - $domain\$env:username"

#Sample function that provides the location of the script
function Get-ScriptDirectory
{
<#
	.SYNOPSIS
		Get-ScriptDirectory returns the proper location of the script.

	.OUTPUTS
		System.String
	
	.NOTES
		Returns the correct path within a packaged executable.
#>
	[OutputType([string])]
	param ()
	if ($hostinvocation -ne $null)
	{
		Split-Path $hostinvocation.MyCommand.path
	}
	else
	{
		Split-Path $script:MyInvocation.MyCommand.Path
	}
}



#region Styles
# Font Styles / Schrift Stile
$bold = New-Object Drawing.Font("Lucida Console", 7, [Drawing.Fontstyle]::Bold)
$norm = New-Object Drawing.Font("Lucida Console", 7, [Drawing.Fontstyle]::Regular)
$log = New-Object Drawing.Font("Lucida Console", 1, [Drawing.Fontstyle]::Regular)
[Drawing.Color]$gray = "Control"
[Drawing.Color]$green = "Green"
[Drawing.Color]$red = "Red"
[Drawing.Color]$black = "Black"
[Drawing.Color]$white = "White"
[Drawing.Color]$blue = "Blue"
$global:Fillchar = 178
$Newline = "`n"
$Newline2 = "`n`n"
#endregion Styles

#Functions
#region Global-Functions

#Sample variable that provides the location of the script
[string]$ScriptDirectory = Get-ScriptDirectory

#region Get-IpConfig
# ==========================================================
# Get-IPConfig.ps1
# Made By : Assaf Miron
#  http://assaf.miron.googlepages.com
# Description : Formats the IP Config information into powershell
# ==========================================================
function Get-IPConfig
{
	param ($Computername = "LocalHost",
		$OnlyConnectedNetworkAdapters = $true
	)
	gwmi -Class Win32_NetworkAdapterConfiguration -ComputerName $ComputerName | Where { $_.IPEnabled -eq $OnlyConnectedNetworkAdapters } | Format-List @{ Label = "Computer Name"; Expression = { $_.__SERVER } }, IPEnabled, Description, MACAddress, IPAddress, IPSubnet, DefaultIPGateway, DHCPEnabled, DHCPServer, @{ Label = "DHCP Lease Expires"; Expression = { [dateTime]$_.DHCPLeaseExpires } }, @{ Label = "DHCP Lease Obtained"; Expression = { [dateTime]$_.DHCPLeaseObtained } }
}
#endregion
#region Get-ComputerTxtBox
function Get-ComputerTxtBox
{ $global:ComputerName = $textbox_computername.Text }
#endregion
#region Add-RichTextBox
# Function - Add Text to RichTextBox
function Add-RichTextBox
{
	[CmdletBinding()]
	param ($text)
	$Fill = "-"
	$Fill = $Fill * $Fillchar
	#$richtextbox_output.Text += "`tCOMPUTERNAME: $ComputerName`n"
	$richtextbox_output.SelectionFont = $log
	$richtextbox_output.SelectionColor = $blue
	$timestamp = Get-Date -Format "dd-MM-yyyy HH:mm:ss`n"
	$richtextbox_output.AppendText($timestamp)
	$richtextbox_output.SelectionFont = $norm
	$richtextbox_output.SelectionColor = $white
	$richtextbox_output.AppendText($text)
	$richtextbox_output.SelectionFont = $bold
	$richtextbox_output.AppendText($Newline)
	$richtextbox_output.AppendText($Fill)
	$richtextbox_output.AppendText($Newline)
}
#Set-Alias artb Add-RichTextBox -Description "Add content to the RichTextBox"
#endregion
#region Add-RichtextBoxTitle
function Add-RichTextBoxTitle
{
	[CmdletBinding()]
	param ($text)
	$Fill = "-"
	$Fill = $Fill * $Fillchar
	#$richtextbox_output.Text += "`tCOMPUTERNAME: $ComputerName`n"
	$richtextbox_output.SelectionFont = $log
	$richtextbox_output.SelectionColor = $Gray
	$timestamp = Get-Date -Format "dd-MM-yyyy HH:mm:ss`n"
	$richtextbox_output.SelectionFont = $bold
	$richtextbox_output.SelectionColor = $white
	$richtextbox_output.SelectionFont = $bold
	$richtextbox_output.AppendText($text)
	$richtextbox_output.SelectionFont = $bold
	$richtextbox_output.AppendText($Newline)
	$richtextbox_output.AppendText($Fill)
	$richtextbox_output.AppendText($Newline)
}
#endregion
#region Add-RichtextBoxWarn
function Add-RichTextBoxWarn
{
	[CmdletBinding()]
	param ($text)
	$Fill = "-"
	$Fill = $Fill * $Fillchar
	#$richtextbox_output.Text += "`tCOMPUTERNAME: $ComputerName`n"
	$richtextbox_output.SelectionFont = $log
	$richtextbox_output.SelectionColor = $Gray
	$timestamp = Get-Date -Format "dd-MM-yyyy HH:mm:ss`n"
	$richtextbox_output.SelectionFont = $norm
	$richtextbox_output.SelectionColor = $Red
	$richtextbox_output.AppendText($text)
	$richtextbox_output.SelectionColor = $Black
	$richtextbox_output.SelectionFont = $bold
	$richtextbox_output.AppendText($Newline)
	$richtextbox_output.AppendText($Fill)
	$richtextbox_output.AppendText($Newline)
	
}
#endregion RichtextBoxWarn
#region Test-PSRemoting

function Test-PSRemoting
{
	Param (
		[alias('dnsHostName')]
		[Parameter(Mandatory = $true, ValueFromPipelineByPropertyName = $true, ValueFromPipeline = $true)]
		[string]$ComputerName
	)
	Process
	{
		Write-Verbose " [Test-PSRemoting] :: Start Process"
		if ($ComputerName -match "(.*)(\$)$")
		{
			$ComputerName = $ComputerName -replace "(.*)(\$)$", '$1'
		}
		
		try
		{
			
			$result = Invoke-Command -ComputerName $computername { 1 } -ErrorAction SilentlyContinue
			
			if ($result -eq 1)
			{
				return $True
			}
			else
			{
				return $False
			}
		}
		catch
		{
			return $False
		}
	}
}

#endregion
#region Remove-MyVM
#requires -version 4.0

Function Remove-MyVM
{
	
<#
 
 .Synopsis
 Remove a Hyper-V Virtual machine including disk files.
 .Description
 This is a wrapper and proxy command that uses Get-VM and Remove-VM to remove a Hyper-V virtual machine as well as its associated virtual disk files. If the virtual machine has snapshots, those will be removed as well.

 Note: There appears to be an issue with the Remove-VM cmdlet. It does not appear to respect the -Confirm parameter. You will most likely always be prompted to remove the virtual machine.
 .Parameter Computername
 The name of the Hyper-V host. You should always specify the name of a remote server, even if piping an expression to this command. This parameter has an alias of CN.
 .Parameter ClusterObject
 Specifies the cluster resource or cluster group of the virtual machine to be retrieved.
 .Parameter Id
 Specifies the identifier of the virtual machine to be retrieved.
 .Parameter Name
 Specifies the name of the virtual machine to be retrieved. This parameter has an alias of VMName.
 
 .Notes
 Version      : 1.0
 Last Modified:	October 21, 2014

 Learn more about PowerShell:
 http://jdhitsolutions.com/blog/essential-powershell-resources/

  ****************************************************************
  * DO NOT USE IN A PRODUCTION ENVIRONMENT UNTIL YOU HAVE TESTED *
  * THOROUGHLY IN A LAB ENVIRONMENT. USE AT YOUR OWN RISK.  IF   *
  * YOU DO NOT UNDERSTAND WHAT THIS SCRIPT DOES OR HOW IT WORKS, *
  * DO NOT USE IT OUTSIDE OF A SECURE, TEST SETTING.             *
  ****************************************************************

 .Example
 PS C:\> Remove-MyVM Test1 -computername chi-hvr2

 .Example
 PS C:\> get-vm test* -computername chi-hvr2 | remove-myvm -computername chi-hvr2 -whatif
 What if: Remove-VMSnapshot will remove snapshot "Test3 - (10/21/2014 - 8:33:29 PM)".
 What if: Performing the operation "Remove File" on target "D:\disks\test3.vhdx".
 What if: Remove-VM will remove virtual machine "Test3".
 What if: Performing the operation "Remove File" on target "D:\Disks\Test2.vhdx".
 What if: Remove-VM will remove virtual machine "Test2".
 What if: Performing the operation "Remove File" on target "D:\Disks\Test1.vhdx".
 What if: Remove-VM will remove virtual machine "Test1".

 Run this command without -Whatif to remove these files and virtual machines.
 .Link
 Get-VM
 Remove-VM
 
 #>
	
	[CmdletBinding(DefaultParameterSetName = 'Name', SupportsShouldProcess = $True)]
	param (
		[Parameter(ParameterSetName = 'Id')]
		[Parameter(ParameterSetName = 'Name')]
		[ValidateNotNullOrEmpty()]
		[Alias('CN')]
		[string]$ComputerName = $env:COMPUTERNAME,
		[Parameter(ParameterSetName = 'Id', Position = 0, ValueFromPipeline = $true, ValueFromPipelineByPropertyName = $true)]
		[ValidateNotNull()]
		[System.Nullable[guid]]$Id,
		[Parameter(ParameterSetName = 'Name', Position = 0, ValueFromPipeline = $true)]
		[Alias('VMName')]
		[ValidateNotNullOrEmpty()]
		[string[]]$Name,
		[Parameter(ParameterSetName = 'ClusterObject', Mandatory = $true, Position = 0, ValueFromPipeline = $true)]
		[PSTypeName('Microsoft.FailoverClusters.PowerShell.ClusterObject')]
		[ValidateNotNullOrEmpty()]
		[psobject]$ClusterObject,
		[Switch]$Passthru
	)
	
	begin
	{
		
		Write-Verbose -Message "Starting $($MyInvocation.Mycommand)"
		Write-Verbose -Message "Using parameter set $($PSCmdlet.ParameterSetName)"
		Try
		{
			$outBuffer = $null
			if ($PSBoundParameters.TryGetValue('OutBuffer', [ref]$outBuffer))
			{
				$PSBoundParameters['OutBuffer'] = 1
			}
			
			#remove Whatif from Boundparameters since Get-VM doesn't recognize it
			$PSBoundParameters.Remove("WhatIf") | Out-Null
			$PSBoundParameters.Remove("Passthru") | Out-Null
			$PSBoundParameters.Remove("Confirm") | Out-Null
			
			$wrappedCmd = $ExecutionContext.InvokeCommand.GetCommand('Get-VM', [System.Management.Automation.CommandTypes]::Cmdlet)
			#$scriptCmd = {& $wrappedCmd @PSBoundParameters }
			
			Write-Verbose "Using parameters:"
			Write-verbose ($PSBoundParameters | Out-String)
			
			$ScriptCmd = {
				
				& $wrappedCmd @PSBoundParameters -pv vm |
				foreach-object -begin {
					#create a PSSession to the computer
					Try
					{
						Write-Verbose "Creating PSSession to $computername"
						$mysession = New-PSSession -ComputerName $computername
						#turn off Hyper-V object caching
						Write-Verbose "Disabling VMEventing on $computername"
						Invoke-Command { Disable-VMEventing -Force -confirm:$False } -session $mySession
					}
					catch
					{
						Throw
					}
					
				} -process {
					
					Write-Debug ($vm | Out-String)
					
					#write the VM to the pipeline if -Passthru was called
					if ($Passthru) { $vm }
					
					Invoke-Command -scriptblock {
						[CmdletBinding(SupportsShouldProcess = $True, ConfirmImpact = 'medium')]
						Param ($VM,
							[string]$VerbosePreference)
						
						$WhatIfPreference = $using:WhatifPreference
						$confirmPreference = $using:ConfirmPreference
						Write-Verbose "Whatif = $WhatifPreference"
						Write-Verbose "ConfirmPreference = $ConfirmPreference"
						
						#set confirm value
						switch ($confirmPreference)
						{
							"high" { $cv = $false }
							"medium" { $cv = $False }
							"low" { $cv = $True }
							Default { $cv = $False }
						}
						
						#remove snapshots first
						#$VM is the pipelinevariable from the wrapped command
						Write-Verbose "Testing for snapshots"
						if (Get-VMSnapshot -VMName $VM.name -ErrorAction SilentlyContinue)
						{
							Write-Verbose "Removing existing snapshots"
							Remove-VMSnapshot -VMName $VM.name -IncludeAllChildSnapshots -Confirm:$cv
						}
						
						$disks = $vm.id | Get-VHD
						
						#remove disks
						foreach ($disk in $disks)
						{
							#the disk path might still reflect a snapshot so clean them up first
							#This code shouldn't be necessary since we are removing snapshots,
							#but just in case...
							
							[regex]$rx = "\.avhd(.?)"
							if ($disk.path -match $rx)
							{
								Write-Verbose "Cleaning up snapshot leftovers"
								#get a clean version of the file extension
								$extension = $rx.Matches($disk.path).value.replace("a", "")
								
								#a regex to find a GUID and file extension
								[regex]$rx = "_(\{){0,1}[0-9a-fA-F]{8}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{4}\-[0-9a-fA-F]{12}(\}){0,1}.avhd(.?)"
								$thePath = $rx.Replace($disk.path, $extension)
							}
							else
							{
								$thePath = $disk.path
							}
							Try
							{
								Write-Verbose "Removing $thePath"
								Remove-Item -Path $thePath -ErrorAction Stop -Confirm:$cv
								$DiskRemove = $True
							}
							Catch
							{
								Write-Warning "Failed to remove $thePath"
								Write-warning $_.exception.message
								#don't continue removing anything if there was a problem removing the disk file
								$DiskRemove = $False
							}
						}
						if ($diskRemove)
						{
							#remove the VM
							$VM | foreach {
								Write-Verbose "Removing virtual machine $($_.name)"
								Remove-VM -Name $_.Name -Confirm:$cv
								#Remove Folder
								Remove-Item -Path $_.Path -Confirm:$CV -Force -Recurse
								
							} #foreach
						} #if disk remove was successful
						else
						{
							Write-Verbose "Aborting virtual machine removal"
						}
						
					} -session $mySession -ArgumentList ($vm, $VerbosePreference) -hidecomputername
					
				} -end {
					#remove PSSession. Ignore -Whatif and always remove it.
					Write-Verbose "Removing PSSession to $computername"
					Remove-PSSession -Session $mySession -WhatIf:$false -Confirm:$False
					
				}
				
			} #scriptCMD
			
			$steppablePipeline = $scriptCmd.GetSteppablePipeline($myInvocation.CommandOrigin)
			$steppablePipeline.Begin($PSCmdlet)
		}
		catch
		{
			throw
		}
	}
	
	process
	{
		try
		{
			$steppablePipeline.Process($_)
		}
		catch
		{
			throw
		}
	}
	
	end
	{
		try
		{
			$steppablePipeline.End()
		}
		catch
		{
			throw
		}
		
		Write-Verbose -Message "Ending $($MyInvocation.Mycommand)"
	}
	
} #end function Remove-MyVM
#endregion
#region Add-RichTextBoxOK
function Add-RichTextBoxOK
{
	[CmdletBinding()]
	param ($text)
	$Fill = "-"
	$Fill = $Fill * $Fillchar
	#$richtextbox_output.Text += "`tCOMPUTERNAME: $ComputerName`n"
	$richtextbox_output.SelectionFont = $log
	$richtextbox_output.SelectionColor = $Gray
	$timestamp = Get-Date -Format "dd-MM-yyyy HH:mm:ss`n"
	$richtextbox_output.SelectionFont = $norm
	$richtextbox_output.SelectionColor = $Green
	$richtextbox_output.AppendText($text)
	$richtextbox_output.SelectionColor = $Black
	$richtextbox_output.AppendText($Newline)
	$richtextbox_output.AppendText($Fill)
	$richtextbox_output.AppendText($Newline)
}
#endregion
#region Get-IP
function Get-IP
{
	
	    <#
	        .Synopsis 
	            Get the IP of the specified host.
	            
	        .Description
	            Get the IP of the specified host.
	            
	        .Parameter ComputerName
	            Name of the Computer to get IP (Default localhost.)
	                
	        .Example
	            Get-IP
	            Description
	            -----------
	            Get IP information the localhost
	            
	            
	        .OUTPUTS
	            PSCustomObject
	            
	        .INPUTS
	            System.String
	        
	        .Notes
	            NAME:      Get-IP
	            AUTHOR:    YetiCentral\bshell
	            Website:   www.bsonposh.com
	            #Requires -Version 2.0
	    #>
	
	[Cmdletbinding()]
	Param (
		[alias('dnsHostName')]
		[Parameter(ValueFromPipelineByPropertyName = $true, ValueFromPipeline = $true)]
		[string]$ComputerName = $Env:COMPUTERNAME
	)
	Process
	{
		$NICs = Get-WmiObject Win32_NetworkAdapterConfiguration -Filter "IPEnabled='$True'" -ComputerName $ComputerName
		foreach ($Nic in $NICs)
		{
			$myobj = @{
				Name = $Nic.Description
				MacAddress = $Nic.MACAddress
				IP4 = $Nic.IPAddress | where{ $_ -match "\d+\.\d+\.\d+\.\d+" }
				IP6 = $Nic.IPAddress | where{ $_ -match "\:\:" }
				IP4Subnet = $Nic.IPSubnet | where{ $_ -match "\d+\.\d+\.\d+\.\d+" }
				DefaultGWY = $Nic.DefaultIPGateway | Select -First 1
				DNSServer = $Nic.DNSServerSearchOrder
				WINSPrimary = $Nic.WINSPrimaryServer
				WINSSecondary = $Nic.WINSSecondaryServer
			}
			$obj = New-Object PSObject -Property $myobj
			$obj.PSTypeNames.Clear()
			$obj.PSTypeNames.Add('BSonPosh.IPInfo')
			$obj
		}
	}
}

#endregion

#PS Deployment Toolkit Functions
#region Function Remove-MSIApplications
Function Remove-MSIApplications
{
<#
.SYNOPSIS
	Removes all MSI applications matching the specified application name.
.DESCRIPTION
	Removes all MSI applications matching the specified application name.
	Enumerates the registry for installed applications matching the specified application name and uninstalls that application using the product code, provided the uninstall string matches "msiexec".
.PARAMETER Name
	The name of the application to uninstall. Performs a regex match on the application display name by default.
.PARAMETER Exact
	Specifies that the named application must be matched using the exact name.
.PARAMETER WildCard
	Specifies that the named application must be matched using a wildcard search.
.PARAMETER Parameters
	Overrides the default parameters specified in the XML configuration file. Uninstall default is: "REBOOT=ReallySuppress /QN".
.PARAMETER AddParameters
	Adds to the default parameters specified in the XML configuration file. Uninstall default is: "REBOOT=ReallySuppress /QN".
.PARAMETER FilterApplication
	Multi-dimensional array that contains property/value/match-type pairs that should be used to filter the list of results returned by Get-InstalledApplication to only those that should be uninstalled.
	Properties that can be filtered upon: ProductCode, DisplayName, DisplayVersion, UninstallString, InstallSource, InstallLocation, InstallDate, Publisher, Is64BitApplication
.PARAMETER ExcludeFromUninstall
	Multi-dimensional array that contains property/value/match-type pairs that should be excluded from uninstall if found.
	Properties that can be excluded: ProductCode, DisplayName, DisplayVersion, UninstallString, InstallSource, InstallLocation, InstallDate, Publisher, Is64BitApplication
.PARAMETER IncludeUpdatesAndHotfixes
	Include matches against updates and hotfixes in results.
.PARAMETER LoggingOptions
	Overrides the default logging options specified in the XML configuration file. Default options are: "/L*v".
.PARAMETER LogName
	Overrides the default log file name. The default log file name is generated from the MSI file name. If LogName does not end in .log, it will be automatically appended.
	For uninstallations, by default the product code is resolved to the DisplayName and version of the application.
.PARAMETER PassThru
	Returns ExitCode, STDOut, and STDErr output from the process.
.PARAMETER ContinueOnError
	Continue if an exit code is returned by msiexec that is not recognized by the App Deploy Toolkit. Default is: $true.
.EXAMPLE
	Remove-MSIApplications -Name 'Adobe Flash'
	Removes all versions of software that match the name "Adobe Flash"
.EXAMPLE
	Remove-MSIApplications -Name 'Adobe'
	Removes all versions of software that match the name "Adobe"
.EXAMPLE
	Remove-MSIApplications -Name 'Java 8 Update' -FilterApplication @(
																		@('Is64BitApplication', $false, 'Exact'),
																		@('Publisher', 'Oracle Corporation', 'Exact')
																	)
	Removes all versions of software that match the name "Java 8 Update" where the software is 32-bits and the publisher is "Oracle Corporation".
.EXAMPLE
	Remove-MSIApplications -Name 'Java 8 Update' -FilterApplication @(,,@('Publisher', 'Oracle Corporation', 'Exact')) -ExcludeFromUninstall @(,,@('DisplayName', 'Java 8 Update 45', 'RegEx'))
	Removes all versions of software that match the name "Java 8 Update" and also have "Oracle Corporation" as the Publisher; however, it does not uninstall "Java 8 Update 45" of the software. NOTE: if only specifying a single array in an array of arrays, the array must be preceded by two commas as in this example.
.EXAMPLE
	Remove-MSIApplications -Name 'Java 8 Update' -ExcludeFromUninstall @(,,@('DisplayName', 'Java 8 Update 45', 'RegEx'))
	Removes all versions of software that match the name "Java 8 Update"; however, it does not uninstall "Java 8 Update 45" of the software. NOTE: if only specifying a single array in an array of arrays, the array must be preceded by two commas as in this example.
.EXAMPLE
	Remove-MSIApplications -Name 'Java 8 Update' -ExcludeFromUninstall @(
																			@('Is64BitApplication', $true, 'Exact'),
																			@('DisplayName', 'Java 8 Update 45', 'Exact'),
																			@('DisplayName', 'Java 8 Update 4*', 'WildCard'),
																			@('DisplayName', 'Java 8 Update 45', 'RegEx')
																		)
	Removes all versions of software that match the name "Java 8 Update"; however, it does not uninstall 64-bit versions of the software, Update 45 of the software, or any Update that starts with 4.
.NOTES
	More reading on how to create arrays if having trouble with -FilterApplication or -ExcludeFromUninstall parameter: http://blogs.msdn.com/b/powershell/archive/2007/01/23/array-literals-in-powershell.aspx
.LINK
	http://psappdeploytoolkit.com
#>
	[CmdletBinding()]
	Param (
		[Parameter(Mandatory = $true)]
		[ValidateNotNullorEmpty()]
		[string]$Name,
		[Parameter(Mandatory = $false)]
		[switch]$Exact = $false,
		[Parameter(Mandatory = $false)]
		[switch]$WildCard = $false,
		[Parameter(Mandatory = $false)]
		[Alias('Arguments')]
		[ValidateNotNullorEmpty()]
		[string]$Parameters,
		[Parameter(Mandatory = $false)]
		[ValidateNotNullorEmpty()]
		[string]$AddParameters,
		[Parameter(Mandatory = $false)]
		[ValidateNotNullorEmpty()]
		[array]$FilterApplication = @(@()),
		[Parameter(Mandatory = $false)]
		[ValidateNotNullorEmpty()]
		[array]$ExcludeFromUninstall = @(@()),
		[Parameter(Mandatory = $false)]
		[switch]$IncludeUpdatesAndHotfixes,
		[Parameter(Mandatory = $false)]
		[ValidateNotNullorEmpty()]
		[string]$LoggingOptions,
		[Parameter(Mandatory = $false)]
		[Alias('LogName')]
		[string]$private:LogName,
		[Parameter(Mandatory = $false)]
		[ValidateNotNullorEmpty()]
		[switch]$PassThru = $false,
		[Parameter(Mandatory = $false)]
		[ValidateNotNullorEmpty()]
		[boolean]$ContinueOnError = $true
	)
	
	Begin
	{
		## Get the name of this function and write header
		[string]${CmdletName} = $PSCmdlet.MyInvocation.MyCommand.Name
		Write-FunctionHeaderOrFooter -CmdletName ${CmdletName} -CmdletBoundParameters $PSBoundParameters -Header
	}
	Process
	{
		## Build the hashtable with the options that will be passed to Get-InstalledApplication using splatting
		[hashtable]$GetInstalledApplicationSplat = @{ Name = $name }
		If ($Exact) { $GetInstalledApplicationSplat.Add('Exact', $Exact) }
		ElseIf ($WildCard) { $GetInstalledApplicationSplat.Add('WildCard', $WildCard) }
		If ($IncludeUpdatesAndHotfixes) { $GetInstalledApplicationSplat.Add('IncludeUpdatesAndHotfixes', $IncludeUpdatesAndHotfixes) }
		
		[psobject[]]$installedApplications = Get-InstalledApplication @GetInstalledApplicationSplat
		
		Write-Log -Message "Found [$($installedApplications.Count)] application(s) that matched the specified criteria [$Name]." -Source ${CmdletName}
		
		## Filter the results from Get-InstalledApplication
		[Collections.ArrayList]$removeMSIApplications = New-Object -TypeName 'System.Collections.ArrayList'
		If (($null -ne $installedApplications) -and ($installedApplications.Count))
		{
			ForEach ($installedApplication in $installedApplications)
			{
				If ($installedApplication.UninstallString -notmatch 'msiexec')
				{
					Write-Log -Message "Skipping removal of application [$($installedApplication.DisplayName)] because uninstall string [$($installedApplication.UninstallString)] does not match `"msiexec`"." -Severity 2 -Source ${CmdletName}
					Continue
				}
				If ([string]::IsNullOrEmpty($installedApplication.ProductCode))
				{
					Write-Log -Message "Skipping removal of application [$($installedApplication.DisplayName)] because unable to discover MSI ProductCode from application's registry Uninstall subkey [$($installedApplication.UninstallSubkey)]." -Severity 2 -Source ${CmdletName}
					Continue
				}
				
				#  Filter the results from Get-InstalledApplication to only those that should be uninstalled
				If (($null -ne $FilterApplication) -and ($FilterApplication.Count))
				{
					Write-Log -Message "Filter the results to only those that should be uninstalled as specified in parameter [-FilterApplication]." -Source ${CmdletName}
					[boolean]$addAppToRemoveList = $false
					ForEach ($Filter in $FilterApplication)
					{
						If ($Filter[0][2] -eq 'RegEx')
						{
							If ($installedApplication.($Filter[0][0]) -match [regex]::Escape($Filter[0][1]))
							{
								[boolean]$addAppToRemoveList = $true
								Write-Log -Message "Preserve removal of application [$($installedApplication.DisplayName) $($installedApplication.Version)] because of regex match against [-FilterApplication] criteria." -Source ${CmdletName}
							}
						}
						ElseIf ($Filter[0][2] -eq 'WildCard')
						{
							If ($installedApplication.($Filter[0][0]) -like $Filter[0][1])
							{
								[boolean]$addAppToRemoveList = $true
								Write-Log -Message "Preserve removal of application [$($installedApplication.DisplayName) $($installedApplication.Version)] because of wildcard match against [-FilterApplication] criteria." -Source ${CmdletName}
							}
						}
						ElseIf ($Filter[0][2] -eq 'Exact')
						{
							If ($installedApplication.($Filter[0][0]) -eq $Filter[0][1])
							{
								[boolean]$addAppToRemoveList = $true
								Write-Log -Message "Preserve removal of application [$($installedApplication.DisplayName) $($installedApplication.Version)] because of exact match against [-FilterApplication] criteria." -Source ${CmdletName}
							}
						}
					}
				}
				Else
				{
					[boolean]$addAppToRemoveList = $true
				}
				
				#  Filter the results from Get-InstalledApplication to remove those that should never be uninstalled
				If (($null -ne $ExcludeFromUninstall) -and ($ExcludeFromUninstall.Count))
				{
					ForEach ($Exclude in $ExcludeFromUninstall)
					{
						If ($Exclude[0][2] -eq 'RegEx')
						{
							If ($installedApplication.($Exclude[0][0]) -match [regex]::Escape($Exclude[0][1]))
							{
								[boolean]$addAppToRemoveList = $false
								Write-Log -Message "Skipping removal of application [$($installedApplication.DisplayName) $($installedApplication.Version)] because of regex match against [-ExcludeFromUninstall] criteria." -Source ${CmdletName}
							}
						}
						ElseIf ($Exclude[0][2] -eq 'WildCard')
						{
							If ($installedApplication.($Exclude[0][0]) -like $Exclude[0][1])
							{
								[boolean]$addAppToRemoveList = $false
								Write-Log -Message "Skipping removal of application [$($installedApplication.DisplayName) $($installedApplication.Version)] because of wildcard match against [-ExcludeFromUninstall] criteria." -Source ${CmdletName}
							}
						}
						ElseIf ($Exclude[0][2] -eq 'Exact')
						{
							If ($installedApplication.($Exclude[0][0]) -eq $Exclude[0][1])
							{
								[boolean]$addAppToRemoveList = $false
								Write-Log -Message "Skipping removal of application [$($installedApplication.DisplayName) $($installedApplication.Version)] because of exact match against [-ExcludeFromUninstall] criteria." -Source ${CmdletName}
							}
						}
					}
				}
				
				If ($addAppToRemoveList)
				{
					Write-Log -Message "Adding application to list for removal: [$($installedApplication.DisplayName) $($installedApplication.Version)]." -Source ${CmdletName}
					$removeMSIApplications.Add($installedApplication)
				}
			}
		}
		
		## Build the hashtable with the options that will be passed to Execute-MSI using splatting
		[hashtable]$ExecuteMSISplat = @{ Action = 'Uninstall'; Path = '' }
		If ($ContinueOnError) { $ExecuteMSISplat.Add('ContinueOnError', $ContinueOnError) }
		If ($Parameters) { $ExecuteMSISplat.Add('Parameters', $Parameters) }
		ElseIf ($AddParameters) { $ExecuteMSISplat.Add('AddParameters', $AddParameters) }
		If ($LoggingOptions) { $ExecuteMSISplat.Add('LoggingOptions', $LoggingOptions) }
		If ($LogName) { $ExecuteMSISplat.Add('LogName', $LogName) }
		If ($PassThru) { $ExecuteMSISplat.Add('PassThru', $PassThru) }
		If ($IncludeUpdatesAndHotfixes) { $ExecuteMSISplat.Add('IncludeUpdatesAndHotfixes', $IncludeUpdatesAndHotfixes) }
		
		If (($null -ne $removeMSIApplications) -and ($removeMSIApplications.Count))
		{
			ForEach ($removeMSIApplication in $removeMSIApplications)
			{
				Write-Log -Message "Remove application [$($removeMSIApplication.DisplayName) $($removeMSIApplication.Version)]." -Source ${CmdletName}
				$ExecuteMSISplat.Path = $removeMSIApplication.ProductCode
				If ($PassThru)
				{
					[psobject[]]$ExecuteResults += Execute-MSI @ExecuteMSISplat
				}
				Else
				{
					Execute-MSI @ExecuteMSISplat
				}
			}
		}
		Else
		{
			Write-Log -Message 'No applications found for removal. Continue...' -Source ${CmdletName}
		}
	}
	End
	{
		If ($PassThru) { Write-Output -InputObject $ExecuteResults }
		Write-FunctionHeaderOrFooter -CmdletName ${CmdletName} -Footer
	}
}
#endregion

#endregion Global-Functions

